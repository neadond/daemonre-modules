#    Daemon Project
#    Copyright (C) 2021-2022 The Authors

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/

# Imports
import wikipedia
from gtts import gTTS

# Locals 
wikipedia_error_msg = 'В энциклопедии нет информации об этом'
wikipedia_url_to_page_msg = 'Ссылка на полную статью:'
probability_word = 'Веротность'
willhappen_verb = 'произойдёт'
what_word = 'что'

async def get_wiki(find_text):
    try:
        wikipedia.set_lang('ru')
        ny = wikipedia.page(find_text)
        wikitext=ny.content[:1000]
        wikimas=wikitext.split('.')
        wikimas = wikimas[:-1]
        wikitext2 = ''
        for x in wikimas:
            if not('==' in x):
                if(len((x.strip()))>3):
                   wikitext2=wikitext2+x+'.'
            else:
                break
        wikitext2=re.sub('\([^()]*\)', '', wikitext2)
        wikitext2=re.sub('\([^()]*\)', '', wikitext2)
        wikitext2=re.sub('\{[^\{\}]*\}', '', wikitext2)
        return(wikitext2 + f'\n{wikipedia_url_to_page_msg} {wikipedia.page(find_text).url}')
    except Exception as e:
        return wikipedia_error_msg
        
@bot.on.message(command='flip')
async def flip(message: Message):
    await addtobd(message.peer_id)
    if await check_bl_wl(message) != False:
        flip = ['🌝 Орёл!', '🌚 Решка!']
        await message.reply(random.choice(flip))  

@bot.on.message(text='/say <saytext>')
async def say(message: Message, saytext):
    if await check_bl_wl(message) != False:
        file_id = time.time()
        st = gTTS(saytext, lang='ru')
        st.save(f'voice_{message.peer_id}_{file_id}.mp3')
        fp = f'voice_{message.peer_id}_{file_id}.mp3'
        audio_message = await VoiceMessageUploader(bot.api).upload(fp, fp, peer_id=message.peer_id)
        await message.reply(attachment=audio_message)
        os.remove(f'voice_{message.peer_id}_{file_id}.mp3')

@bot.on.message(text='/wiki <find_text>')    
async def search_in_wikipedia(message: Message, find_text):
    if await check_bl_wl(message) != False:
        await message.reply(await get_wiki(find_text))

@bot.on.message(text='/chance <text>')
async def chance_command(message: Message, text):
    procent = random.randint(0, 100)
    await message.reply(f'{probability_word} {what_word} {text} {willhappen_verb} {procent}%')
    await update_stats(message)

@bot.on.message(text='/jurik <txt>')
async def jurik(message: Message, txt):
    file_id = time.time()
    with open(f'{config.modules_dir}/zhirinovsky/zhirinovsky.jpeg', 'rb') as f:
        blob_flie = f.read()
        txt = re.sub('^(ЕСТЬ ИДЕЯ|МБ|МОЖЕТ БЫТЬ|ПРЕДЛАГАЮ|А МОЖЕТ|МОЖЕТ|ДАВАЙТЕ|ДАВАЙ) ', '', txt, flags=re.IGNORECASE)
        with Image(blob=blob_flie) as img:
            with Image(width=560, height=360) as img2:
                img2.options['pango:wrap'] = 'word-char'
                img2.options['pango:single-paragraph'] = 'false'
                img2.font = Font('OswaldRegular')
                img2.font_color = '#000000'
                img2.font_size = 44
                img2.pseudo(560, 360, pseudo=f'pango:{txt}')
                img.composite(image=img2, left=50, top=630)
            img.merge_layers('flatten')
            img.format = 'jpeg'
            with img.clone() as liquid:
                liquid.save(filename=f'{config.modules_dir}/zhirinovsky/jurik_{message.peer_id}_{file_id}.jpg')
        mettionsphoto = await PhotoMessageUploader(bot.api).upload(f'{config.modules_dir}/zhirinovsky/jurik_{message.peer_id}_{file_id}.jpg')
        await message.reply(attachment=mettionsphoto)
        os.remove(f'{config.modules_dir}/zhirinovsky/jurik_{message.peer_id}_{file_id}.jpg')
        await update_stats(message)
